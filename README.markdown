# What is the FLA

This is the **development** repository of FSFE’s [Fiduciary License Agreement][fla].

While the text of the FLA-2.0 is indeed under the CA-BY-SA-3.0 and you may fork it to write your own CLA, if you change the text of the FLA in any way, you should not call it the FLA, but choose a different name to avoid confusion. If you have any questions regarding regarding this, contact the [FSFE Legal Team][lt].


# How to apply the FLA to your own project

The FLA text in this repository is **not** intended to be used in your own project directly.

The recommended way to use the FLA is to go to [ContributorAgreements.org][cao] and use the [CLA chooser][chooser] there. When you do so, simply keep the default “FLA recommended by FSFE” selected and fill in the rest of the form in order to generate the needed document(s).

For more information see the [FSFE’s official website on the FLA][fla].

[fla]: https://fsfe.org/activities/ftf/fla.en.html
[cao]: http://contributoragreements.org/
[chooser]: http://contributoragreements.org/ca-cla-chooser/
[lt]: https://fsfe.org/activities/ftf/activities.en.html
